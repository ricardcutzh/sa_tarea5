const gulp = require('gulp');
const zip = require('gulp-zip');
const fileindex = require('gulp-fileindex');

gulp.task('default1', function(){
    return gulp.src('./Aviso_Piloto/**')
           .pipe(zip('Servicio_Aviso.zip'))
           .pipe(gulp.dest('./dist'));
});

gulp.task('default2', function(){
    return gulp.src('./Rastreo/**')
           .pipe(zip('Servicio_Rastreo.zip'))
           .pipe(gulp.dest('./dist'));
})

gulp.task('default3', function(){
    return gulp.src('./Solicitud_Cliente/**')
           .pipe(zip('Servicio_Cliente.zip'))
           .pipe(gulp.dest('./dist'));
})


gulp.task('index', function(){
    return gulp.src('./dist/*.zip')
          .pipe(fileindex())
          .pipe(gulp.dest('./dist'));
});


gulp.task('default', gulp.series('default1', 'default2', 'default3','index'));